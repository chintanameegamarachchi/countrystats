import { TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';

import { DataService } from './data.service';
import { Country } from './models/country.model';

describe('DataService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [ HttpClientModule ]
  }));

  it('should return list of countries', (done: DoneFn) => {
    const dataService = TestBed.get(DataService);

    dataService.getCountries().subscribe((countries: Country[]) => {
      expect(countries.length > 0);
      done();
    });
  });
});
