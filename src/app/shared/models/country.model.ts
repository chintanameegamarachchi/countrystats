export class Country {
    name: string;
    short_code: string;
    capital: string;
    region: string;
    area: number;
    nativename: string;
    currencies: string[] = new Array<string>()
}