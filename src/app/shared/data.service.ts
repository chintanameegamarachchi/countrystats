import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Country } from './models/country.model';

const mapCountries = (response: any[]) => {
  var countries: Array<Country> = new Array<Country>();
    
  for (let country of response){
    var newCountry = new Country();
    newCountry.name = country.name;
    newCountry.short_code = country.alpha2Code;
    newCountry.capital = country.capital;
    newCountry.region = country.region;
    newCountry.area = country.area;
    newCountry.nativename = country.nativeName;
    
    for (let c of country.currencies){
      newCountry.currencies.push(c.name);
    }
    
    //newCountry.short_code = ;
    countries.push(newCountry);
  }

  return countries;
};

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private _httpClient: HttpClient ) { }

  public getCountries(): Observable<Country[]> {

    var result = this._httpClient.get("https://restcountries.eu/rest/v2/all").pipe(map(mapCountries));

    return result;
  }
}
