import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';
import { HomeComponent } from './home/home.component';
import { EditComponent } from './edit/edit.component';
import { AboutComponent } from './about/about.component';

const routes: Routes = [
  { 
    path: '',
    component: LayoutComponent,
    children: [ 
      { path: 'home', component: HomeComponent }, 
      { path: 'edit', component: EditComponent }, 
      { path: 'about', component: AboutComponent }, 
      { path: '**', redirectTo: 'home' }]
  } ]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UiRoutingModule {}
