import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UiRoutingModule } from './ui-routing.module';
import { LayoutComponent } from './layout/layout.component';
import { HomeComponent } from './home/home.component';
import { EditComponent } from './edit/edit.component';
import { AboutComponent } from './about/about.component';
import { CountryStatComponent } from './country-stat/country-stat.component';

@NgModule({
  declarations: [LayoutComponent, HomeComponent, EditComponent, AboutComponent, CountryStatComponent],
  imports: [
    CommonModule,
    UiRoutingModule
  ]
})
export class UiModule { }
