import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/shared/data.service';
import { Country } from 'src/app/shared/models/country.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private _dataSvc: DataService) { }

  countries: Country[] = new Array<Country>();

  ngOnInit() {
    this._dataSvc.getCountries().subscribe(c => this.countries = c);
  }

}
