import { Component, OnInit, Input } from '@angular/core';
import { Country } from 'src/app/shared/models/country.model';

@Component({
  selector: 'app-country-stat',
  templateUrl: './country-stat.component.html',
  styleUrls: ['./country-stat.component.scss']
})
export class CountryStatComponent implements OnInit {

  @Input() country: Country;

  constructor() { }

  ngOnInit() {
  }

}
